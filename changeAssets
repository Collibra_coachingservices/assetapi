//****************************************************************************************************
//***
//*** API: 					AssetApi 
//*** Method: 				changeAssets(List<ChangeAssetRequest> changeAssetRequests)
//*** Returns: 				List<Asset>
//*** Description: 			Changes multiple assets.
//*** Required Parameters: 	a list of ChangeAssetRequest Objects
//*** Use Case: 			Changing a list of assets to a new domain and status
//***
//****************************************************************************************************

//import the class to the ChangeAssetRequest  object
//not necessary if using builders.get("ChangeAssetRequest")
import com.collibra.dgc.core.api.dto.instance.asset.ChangeAssetRequest

//*** Defining the variables for the example:
//*** id = the current asset's ID
def ids = ["e35f2fa6-a912-41c1-8ae9-601457182b6b","9c14f1db-e15d-44ad-9581-86b752a0aa04","de283bde-1c98-4882-942d-db1503352c9a"]
//*** newDomainId - the domain where asset will be moved to: OOTB "New Business Terms" Domain
def newDomainId = string2Uuid("00000000-0000-0000-0000-000000006013")
//*** newStatusId - the ID of the new Status - retrieved using statusApi
def newStatusId = statusApi.getStatusByName("Accepted").getId()

//*** defining an empty list
def changeAssetList = []

//*** Defining the changeAssetList with ChangeAssetRequest object
//*** Example A - by using builder()
for(id in ids){
changeAssetList.add(ChangeAssetRequest.builder()
					.id(string2Uuid(id.toString())) 	//required
					.domainId(newDomainId)				//optional - move to new domain
					.statusId(newStatusId)				//optional - change the status
					.build()
					)
}

//*** Defining the changeAssetList with ChangeAssetRequest
//*** Example B - by using constructor
for(id in ids){
	changeAssetRequest = new ChangeAssetRequest()
	changeAssetRequest.setId(string2Uuid(id.toString()))	//required
	changeAssetRequest.setDomainId(newDomainId)				//optional - move to new domain
	changeAssetRequest.setStatusId(newStatusId)				//optional - change the status
	changeAssetList.add(changeAssetRequest)
}			
//*** running the changeAssets Method:
assetApi.changeAssets(changeAssetList)