//****************************************************************************************************
//***
//*** API: 					AssetApi 
//*** Method: 				findAssets(FindAssetsRequest findAssetsRequest)
//*** Returns: 				PagedResponse<Asset>
//*** Description: 			Returns assets matching the given search criteria.
//*** Required Parameters: 	FindAssetsRequest Object
//*** Use Case: 			Find all assets within a certain domain, of a particular type and status.
//***
//****************************************************************************************************

//import the class to the FindAssetsRequest object
//not necessary if using builders.get("FindAssetsRequest")
import com.collibra.dgc.core.api.dto.instance.asset.FindAssetsRequest

//*** Defining the variables for the example:
//*** domainId - the domain that will be searched for assets: OOTB "New Business Terms" Domain
def domainId = string2Uuid("00000000-0000-0000-0000-000000006013")
//*** typeId - the asset type that will be searched for - OOTB "Business Term" Asset Type
def typeId = string2Uuid("00000000-0000-0000-0000-000000011001")
//*** statusId - the ID of the Status - retrieved using statusApi
def statusId = statusApi.getStatusByName("Accepted").getId()

//*** Defining the FindAssetsRequest object
//*** Example A - by using builder()
findAssetsRequest = FindAssetsRequest.builder()
					.typeIds([typeId])
					.domainId(domainId)
					.statusIds([statusId])
					.build()

//*** Defining the FindAssetsRequest
//*** Example B - by using constructor
findAssetsRequest = new FindAssetsRequest()
findAssetsRequest.setTypeIds([typeId])	
findAssetsRequest.setDomainId(domainId)
findAssetsRequest.setStatusIds([statusId])
					
//*** running the findAssets Method:
assetsFound = assetApi.findAssets(findAssetsRequest)	//returns a pagedResponse
assetsFoundList = assetsFound.getResults()				//converts to a list of assets
loggerApi.info("assetsFoundList: ${assetsFoundList}")