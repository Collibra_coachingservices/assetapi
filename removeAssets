//****************************************************************************************************
//***
//*** API: 					AssetApi 
//*** Method: 				removeAssets(List<UUID> assetIds)
//*** Returns: 				void	
//*** Description: 			Removes multiple assets.
//*** Required Parameters: 	List of Asset UUID object
//*** Use Case: 			Remove all assets in current domain.
//***
//****************************************************************************************************

//import the class to the FindAssetsRequest object
//not necessary if using builders.get("FindAssetsRequest")
import com.collibra.dgc.core.api.dto.instance.asset.FindAssetsRequest

//*** Defining the variables for the example:
//*** domainId - the Id of current domain
def domainId = itemV2.getDomainId()
	
//*** Find the Assets with assetApi.findAssets
//*** Define the by findAssetsRequest using builder()
findAssetsRequest = FindAssetsRequest.builder().domainId(domainId).build()
assetsFound = assetApi.findAssets(findAssetsRequest).getResults() 	//returns a list<Asset>
assetIds = assetsFound.collect{it.getId()} 						 	//converts list<Asset> to list<UUID>

//*** running the removeAssets Method to remove the assets
assetApi.removeAssets(assetIds)
